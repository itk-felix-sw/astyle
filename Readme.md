# ASTYLE

This is a package downloaded from sourceforge and required by Quasar to beautify the code.
To change the downloaded version use the following guideline

```
wget https://sourceforge.net/projects/astyle/files/astyle/astyle%203.1/astyle_3.1_linux.tar.gz/download
mv download astyle_3.1_linux.tar.gz
```

